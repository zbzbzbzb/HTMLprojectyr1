function checkMyForm() {
    beenPressed = false;
    var availableDiv = document.getElementById('js_availability');//gets availibilty text
    if (beenPressed) {
        
        while (availableDiv.firstChild) {
            availableDiv.removeChild(availableDiv.firstChild);
        }

        var HTML = '<p>';//adds <p> tag beforehand
        HTML += "The form is being submitted, <strong>Please Wait</strong>";//sets text
        availableDiv.insertAdjacentHTML("beforeend", HTML + '</p>');//adds text

        availableDiv.firstChild.style.color = "lightblue";

    } else {
        var name = document.getElementById('contact-name');//gets form inputs
        var email = document.getElementById('contact-email');//gets form inputs
        var phone = document.getElementById('contact-phone');//gets form inputs
        var message = document.getElementById('contact-message');//gets form inputs

        if (name.value == "" || email.value == "" || phone.value == "" || message.value == "") {//checks if inputs are empty
            newsize = parseInt(window.getComputedStyle(availableDiv.firstChild).fontSize) + 3;//gets font size parses it to an int and adds 3
            while (availableDiv.firstChild) {//removes child elements usually on one element is here
                availableDiv.removeChild(availableDiv.firstChild);
            }
            //$('#js_availability').css('justify-content','flex-start');//removes centering so the animation fills the whole div
            var js_availability = document.getElementById("js_availability");
            js_availability.style["justify-content"] = "flex-start";
            var HTML = '<p class="warning-text">';//adds <p> tag beforehand
            HTML += "Please fill in all the <strong>FIELDS</strong>";//sets text
            availableDiv.insertAdjacentHTML("beforeend", HTML + '</p>');//adds text

            availableDiv.firstChild.style.fontSize = newsize.toString(10) + "px";//converts newsize to string and chnages font-size
        } else if (isAvailable()) {
            document.getElementById("submit").click();//Presses the hidden submit button so the form goes off
        } else if (confirm("I am sleeping or gaming, are you sure you want to contact me?")) {
            document.getElementById("submit").click();//Presses the hidden submit button so the form goes off
        }
    }

}
