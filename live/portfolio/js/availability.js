var availableDiv = document.getElementById('js_availability');

while (availableDiv.firstChild) {
    availableDiv.removeChild(availableDiv.firstChild);
}

var HTML = '<p>';

if (isAvailable()) {
    HTML += 'You can contact me now';
} else {
    HTML += 'Go to sleep';
}

availableDiv.insertAdjacentHTML("beforeend", HTML + '</p>');


function isAvailable() {
    var time = new Date();

    var morning = 7;
    var night = 23;

    if (time.getHours() > morning && time.getHours() < night) {
        return true;
    } else {
        return false;
    }

}